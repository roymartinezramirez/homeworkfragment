package com.cynergy.homeworkfragments.Model

data class CarEntity (
    var Photo: Int,
    var Model: String,
    var Acceleration: String,
    var StarSafety: String,
    var Range: String,
    var Rooms: String,
    var Price: String
)