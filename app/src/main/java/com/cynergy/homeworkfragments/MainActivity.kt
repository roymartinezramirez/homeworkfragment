package com.cynergy.homeworkfragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.cynergy.homeworkfragments.Fragments.CarDetailFragment
import com.cynergy.homeworkfragments.Fragments.CarFragment
import com.cynergy.homeworkfragments.Listener.OnCarListener
import com.cynergy.homeworkfragments.Model.CarEntity

class MainActivity : AppCompatActivity(), OnCarListener {

    private lateinit var carFragment: CarFragment
    private lateinit var carDetailFragment: CarDetailFragment
    private lateinit var fragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentManager = supportFragmentManager
        carFragment = fragmentManager.findFragmentById(R.id.fragCar) as CarFragment
        carDetailFragment =
            fragmentManager.findFragmentById(R.id.fragCarDetail) as CarDetailFragment
    }

    override fun selectedItemCar(car: CarEntity) {
        carDetailFragment.renderCar(car)
    }

    override fun renderFirst(car: CarEntity?) {
        car?.let {
            selectedItemCar(it)
        }
    }
}