package com.cynergy.homeworkfragments.Listener

import com.cynergy.homeworkfragments.Model.CarEntity

interface OnCarListener {
    fun selectedItemCar(carEntity: CarEntity)
    fun renderFirst(carEntity: CarEntity?)
}