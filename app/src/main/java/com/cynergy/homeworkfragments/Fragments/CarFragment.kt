package com.cynergy.homeworkfragments.Fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cynergy.homeworkfragments.CarAdapter
import com.cynergy.homeworkfragments.Listener.OnCarListener
import com.cynergy.homeworkfragments.Model.CarEntity
import com.cynergy.homeworkfragments.R
import kotlinx.android.synthetic.main.fragment_car.*

class CarFragment : Fragment() {

    private var listener: OnCarListener? = null
    private var cars = mutableListOf<CarEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_car, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()
        context?.let {
            lvCars.adapter = CarAdapter(it,cars)
        }

        lvCars.setOnItemClickListener { _, _, i, _ ->
            listener?.let {
                it.selectedItemCar(cars[i])
            }
        }

        listener?.renderFirst(firstCar())
    }

    private fun firstCar(): CarEntity?{
        cars?.let {
            return it[0]
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnCarListener){
            listener = context
        } else{
            throw RuntimeException("$context must implement OnCarListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setData() {
        cars.apply{
            this.add(CarEntity(R.drawable.tesla_model_s,
            "Model S", "2.3s","*****","402mi","4","$62,420"))
            this.add(CarEntity(R.drawable.tesla_model_3,
            "Model 3", "3.1s","*****","353mi","4","$30,190"))
            this.add(CarEntity(R.drawable.tesla_model_x,
            "Model X","2.6s","*****","371mi","6","$73,190"))
            this.add(CarEntity(R.drawable.tesla_model_y,
            "Model Y","3.5s","*****","326mi","7","$42,190"))
        }
    }

    /*
    "https://drive.google.com/file/d/1TACEOHDMApjP6WTPjdspqoTq7VCs-xQj/view?usp=sharing"
     */
}