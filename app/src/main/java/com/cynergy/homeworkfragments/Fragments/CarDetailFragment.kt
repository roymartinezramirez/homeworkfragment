package com.cynergy.homeworkfragments.Fragments

import android.os.Bundle
import android.view.Display
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cynergy.homeworkfragments.Model.CarEntity
import com.cynergy.homeworkfragments.R
import kotlinx.android.synthetic.main.fragment_car_detail.*

class CarDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_car_detail, container, false)
    }

    fun renderCar(carEntity: CarEntity){
        val Photo = carEntity.Photo
        val Model = carEntity.Model
        val Acceleration = carEntity.Acceleration
        val StarSafety = carEntity.StarSafety
        val Range = carEntity.Range
        val Rooms = carEntity.Rooms
        val Price = carEntity.Price

        ivCar.setImageResource(Photo)
        tvModel.text = Model
        tvAcceleration.text = Acceleration
        tvStars.text = StarSafety
        tvRange.text = Range
        tvRooms.text = Rooms
        tvPrice.text = Price
    }


}