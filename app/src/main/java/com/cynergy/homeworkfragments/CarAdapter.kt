package com.cynergy.homeworkfragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.cynergy.homeworkfragments.Model.CarEntity

class CarAdapter(private val context:Context,private  val cars: List<CarEntity>):BaseAdapter() {
    override fun getCount(): Int {
        return cars.size
    }

    override fun getItem(position: Int) = cars[position]

    override fun getItemId(p0: Int): Long = 0

    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.item_car,null)
        val imgCar = container.findViewById<ImageView>(R.id.ivItemPhoto)
        val tvMyName = container.findViewById<TextView>(R.id.tvItemName)

        val carEntity = cars[position]

        tvMyName.text = carEntity.Model
        imgCar.setImageResource(carEntity.Photo)

        return container

    }
}